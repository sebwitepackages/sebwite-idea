<?php

namespace Sebwite\Tests\Idea;

abstract class TestCase extends \Sebwite\Testbench\TestCase
{
    /**
     * {@inheritdoc}
     */
    protected function getServiceProviderClass()
    {
        return \Sebwite\Idea\IdeaServiceProvider::class;
    }

   /**
    * {@inheritdoc}
    */
    protected function getPackageRootPath()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '..';
    }
}
