<?php

return [
    'console_prefix'         => 'idea',
    'path'                   => '.idea',
    'git_vcs_search_folders' => [ 'workbench', 'app' ],
    'resource_folders'       => [
        // namespace => glob search pattern (relative to workbench folder)
        // glob pattern accepts braces and is recursive
        'config' => [ 'glob' => 'workbench/*/*/config' ],
        'views'  => [ 'glob' => 'workbench/*/*/resources/views', 'to' => '$1-$2' ],
        'assets' => [ 'glob' => 'workbench/*/*/resources/assets', 'to' => '$1-$2' ]
    ],
    'folders'                => [
        [
            // Uses composer.json autoload in the glob paths to resolve namespaces and
            'type' => \Sebwite\Idea\Components\Folders::SOURCE,
            'glob' => 'workbench/*/*',
            // 'dir' => 'src',
            // 'name' => '$1\$2' // Vendor\Package
        ],
        [
            'type'      => \Sebwite\Idea\Components\Folders::RESOURCE,
            'resources' => [
                'config' => [ 'glob' => 'workbench/*/*/config' ],
                'views'  => [ 'glob' => 'workbench/*/*/resources/views', 'to' => '$1-$2' ],
                'assets' => [ 'glob' => 'workbench/*/*/resources/assets', 'to' => '$1-$2' ],
                'themes' => [ 'glob' => 'workbench/*/*/themes/*/*/*/{views,assets}' ]
            ]
        ]
    ],
    'vcs'                    => [

    ],
    'meta'                   => [

    ],
    'laravel'                => [

    ]
];
