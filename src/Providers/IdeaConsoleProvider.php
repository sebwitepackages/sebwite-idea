<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * MIT License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Idea\Providers;

use Sebwite\Support\Console\ConsoleServiceProvider;

/**
 * This is the class IdeaConsoleProvider.
 *
 * @package        Sebwite\Workbench
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
class IdeaConsoleProvider extends ConsoleServiceProvider
{
    protected $dir = __DIR__;

    protected $namespace = 'Sebwite\\Idea\\Console';

    protected $prefix = 'command.workbench.';

    protected $finder = true;
    protected $reject = [ 'BaseCommand' ];
}
