<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Console;

use Sebwite\Idea\Contracts\Idea;
use Sebwite\Support\Console\Command;
use Sebwite\Support\Filesystem;
use Sebwite\Support\Str;

class BaseCommand extends Command
{
    protected $idea;

    /**
     * @var \Sebwite\Support\Filesystem
     */
    protected $fs;

    public function __construct(Idea $idea, Filesystem $fs)
    {
        $this->idea      = $idea;
        $this->fs        = $fs;
        $prefix          = config('idea.console_prefix');
        $this->signature = Str::ensureLeft($this->signature, $prefix . ':');
        parent::__construct();
    }

    /**
     * @return \Sebwite\Idea\Contracts\Idea|\Sebwite\Idea\Idea
     */
    public function idea()
    {
        return $this->idea;
    }

    /**
     * Set the idea value
     *
     * @param \Sebwite\Idea\Contracts\Idea $idea
     *
     * @return BaseCommand
     */
    public function setIdea(Idea $idea)
    {
        $this->idea = $idea;

        return $this;
    }

    /**
     * @return Filesystem
     */
    public function fs()
    {
        return $this->fs;
    }

    /**
     * Set the fs value
     *
     * @param Filesystem $fs
     *
     * @return BaseCommand
     */
    public function setFs($fs)
    {
        $this->fs = $fs;

        return $this;
    }
}
