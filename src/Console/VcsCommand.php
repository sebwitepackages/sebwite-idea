<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Console;

class VcsCommand extends BaseCommand
{
    protected $signature = 'vcs
                                   {--list|-l : List all currently configured folders}';

    public function handle()
    {
        if ($this->option('list')) {

        }
    }

    protected function listFolders()
    {

        $folders = $this->idea()->folders;
        $found   = [ ];
        foreach ($folders->all()->toArray() as $folder) {
            $found[] = [$folder['prefix'], $folders['type'], $folders['path']];
        }
        $this->table([ 'Prefix', 'Type', 'Path' ], $found);
    }

    protected function addFolders()
    {

        $folders = $this->idea()->folders;
        $found   = [ ];
        foreach (config('idea.resource_folders') as $prefix => $globs) {
            foreach ($globs as $glob) {
                foreach ($this->fs()->globule($glob) as $path) {
                    if (!$folders->hasPath($path)) {
                        $found[] = [ $prefix, $path, $glob ];
                        $folders->push($prefix, $path, $folders::RESOURCE);
                    }
                }
            }
        }
        $this->table([ 'Prefix', 'Path', 'Glob' ], $found);
        if ($this->confirm('The table shows the result of your configured [idea.resource_folders] config. Should i apply these to Idea?', true)) {
            $folders->save();
            $this->success('Configuration saved');
        }
    }
}
