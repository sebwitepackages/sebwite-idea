<?php

namespace Sebwite\Idea;

use Sebwite\Support\ServiceProvider;

/**
* The main service provider
*
* @author        Sebwite
* @copyright  Copyright (c) 2015, Sebwite
* @license      http://mit-license.org MIT
* @package      Sebwite\Idea
*/
class IdeaServiceProvider extends ServiceProvider
{
    protected $dir = __DIR__;

    protected $configFiles = [ 'idea' ];

    protected $providers = [
        Providers\IdeaConsoleProvider::class
    ];

    protected $singletons = [
        'sebwite.idea' => Idea::class
    ];

    protected $aliases = [
        'sebwite.idea' => Contracts\Idea::class
    ];

    public function boot()
    {
        $app = parent::boot();
    }

    public function register()
    {
        $app = parent::register();
        Idea::extend('folders', Components\Folders::class);
    }
}
