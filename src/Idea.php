<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea;

use Illuminate\Contracts\Container\Container;
use Sebwite\Idea\Contracts\Idea as IdeaContract;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\Extendable;

/**
 * This is the class Idea.
 *
 * @package        Sebwite\Idea
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 *
 * @property Components\Folders           $folders
 */
class Idea implements IdeaContract
{
    use Extendable;

    /**
     * @var \Illuminate\Contracts\Container\Container
     */
    protected $container;

    /**
     * @var string
     */
    protected $projectPath;

    /**
     * @var string
     */
    protected $ideaPath;

    /**
     * Phpstorm constructor.
     *
     * @param \Illuminate\Contracts\Container\Container  $container
     * @param \Sebwite\Phpstorm\Contracts\MetaRepository $metas
     */
    public function __construct(Container $container)
    {
        $this->container   = $container;
        $this->projectPath = $this->config('project_path');
        $this->ideaPath    = $this->config('idea_path');
    }

    /**
     * config method
     *
     * @param      $key
     * @param null $default
     *
     * @return mixed
     */
    public function config($key, $default = null)
    {
        return array_get($this->container->make('config')->get('phpstorm'), $key, $default);
    }

    public function getProjectName()
    {
        return $this->container->make('files')->get(
            Path::join($this->getIdeaPath(), '.name')
        );
    }

    /**
     * get projectPath value
     *
     * @return mixed
     */
    public function getProjectPath()
    {
        return $this->projectPath;
    }

    /**
     * Set the projectPath value
     *
     * @param mixed $projectPath
     *
     * @return Phpstorm
     */
    public function setProjectPath($projectPath)
    {
        $this->projectPath = $projectPath;

        return $this;
    }

    /**
     * get ideaPath value
     *
     * @return mixed
     */
    public function getIdeaPath()
    {
        return $this->ideaPath;
    }

    /**
     * Set the ideaPath value
     *
     * @param mixed $ideaPath
     *
     * @return Phpstorm
     */
    public function setIdeaPath($ideaPath)
    {
        $this->ideaPath = $ideaPath;

        return $this;
    }


    /**
     * getContainer method
     *
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }
}
