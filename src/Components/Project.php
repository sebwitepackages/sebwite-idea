<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Components;

use Sebwite\Support\Traits\Extendable;

class Project extends Component
{
    use Extendable;

    protected function getFileName()
    {
        return 'workspace.xml';
    }

    protected function getRootName()
    {
        return 'project';
    }
}
