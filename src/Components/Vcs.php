<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Components;

class Vcs extends Component
{
    protected function getFileName()
    {
        return 'vcs.xml';
    }

    protected function getRootName()
    {
        return 'project';
    }

    protected function getTemplate()
    {
        return <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="VcsDirectoryMappings">
    <mapping directory="" vcs="Git" />
  </component>
</project>
EOT;
    }


    /**
     * getMappings method
     *
     * @return \SimpleXMLElement
     */
    protected function getMappings()
    {
        return $this->getComponent('VcsDirectoryMappings', true);
    }

    /**
     * hasMappedDirectories method
     *
     * @return bool
     */
    protected function hasMappedDirectories()
    {
        return $this->getMappings()->children()->count() > 0;
    }

    /**
     * add method
     *
     * @param        $path
     * @param string $vcs
     * @return self
     */
    public function add($path, $vcs = 'git')
    {
        $mapping = $this->getMappings()->addChild('mapping');
        $mapping->addAttribute('directory', $this->parsePath($path));
        $mapping->addAttribute('vcs', ucfirst(strtolower($vcs)));

        return $this;
    }

    /**
     * has method
     *
     * @param $path
     * @return bool
     */
    public function has($path)
    {
        return isset($this->all()[ $path ]);
    }

    /**
     * get method
     *
     * @param $path
     * @return mixed
     */
    public function get($path)
    {
        return $this->all()[ $path ];
    }

    /**
     * all method
     *
     * @return array
     */
    public function all()
    {
        $all = [ ];
        foreach ($this->getMappings()->xpath('//mapping') as $mapping) {
            $map                                          = $this->getAttributes($mapping);
            $all[ $this->cleanPath($map[ 'directory' ]) ] = strtolower($map[ 'vcs' ]);
        }

        return $all;
    }

    /**
     * remove method
     *
     * @param $path
     * @return self
     */
    public function remove($path)
    {
        if ($this->has($path)) {
            $path = $this->getMappings()->xpath('//mapping[@directory="' . $this->parsePath($path) . '"]');
            unset($path[0][0]);
        }

        return $this;
    }
}
