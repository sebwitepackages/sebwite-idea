<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Components;

class Blade extends Component
{

    protected function getFileName()
    {
        // TODO: Implement getFileName() method.
    }

    protected function getRootName()
    {
        // TODO: Implement getRootName() method.
    }

    protected function getTemplate()
    {
        return <<<'EOF'
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="BladeInjectionConfiguration" rawTextEnd="!!}" rawTextStart="{!!">
    <customDirectives>
      <data directive="@endif" />
      <data directive="@while" injection="true" prefix="&lt;?php while (" suffix="): ?&gt;" />
      <data directive="@endminify" />
      <data directive="@unless" injection="true" prefix="&lt;?php if ( ! (" suffix=")): ?&gt;" />
      <data directive="@elseif" injection="true" prefix="&lt;?php elseif (" suffix="): ?&gt;" />
      <data directive="@show" />
      <data directive="@stack" injection="true" prefix="&lt;?php echo" suffix="; ?&gt;" />
      <data directive="@else" />
      <data directive="@choice" injection="true" prefix="&lt;?php echo \Illuminate\Support\Facades\Lang::choice(" suffix="); ?&gt;" />
      <data directive="@can" />
      <data directive="@endforeach" />
      <data directive="@parent" />
      <data directive="@lang" injection="true" prefix="&lt;?php echo \Illuminate\Support\Facades\Lang::get(" suffix="); ?&gt;" />
      <data directive="@for" injection="true" prefix="&lt;?php for (" suffix="): ?&gt;" />
      <data directive="@endforelse" />
      <data directive="@endembed" />
      <data directive="@endsection" />
      <data directive="@macrodef" injection="true" />
      <data directive="@if" injection="true" prefix="&lt;?php if (" suffix="): ?&gt;" />
      <data directive="@yield" injection="true" prefix="&lt;?php echo $__env-&gt;yieldContent(" suffix="); ?&gt;" />
      <data directive="@section" injection="true" prefix="&lt;?php $__env-&gt;startSection(" suffix="); ?&gt;" />
      <data directive="@minify" injection="true" prefix="&lt;?php echo" suffix="; ?&gt;" />
      <data directive="@push" injection="true" prefix="&lt;?php echo" suffix="; ?&gt;" />
      <data directive="@extends" injection="true" prefix="&lt;?php echo $__env-&gt;make(" suffix=", array_except(get_defined_vars(), array('__data', '__path')))-&gt;render(); ?&gt;" />
      <data directive="@embed" injection="true" prefix="&lt;?php echo $__env-&gt;make(" suffix="); ?&gt;" />
      <data directive="@forelse" injection="true" prefix="&lt;?php foreach(" suffix="): ?&gt;" />
      <data directive="@append" />
      <data directive="@endfor" />
      <data directive="@continue" />
      <data directive="@foreach" injection="true" prefix="&lt;?php foreach(" suffix="): ?&gt;" />
      <data directive="@break" />
      <data directive="@macro" injection="true" prefix="&lt;?php echo $__env-&gt;make(" suffix="); ?&gt;" />
      <data directive="@endmarkdown" />
      <data directive="@endcan" />
      <data directive="@unset" injection="true" prefix="&lt;?php" suffix="?&gt;" />
      <data directive="@each" injection="true" prefix="&lt;?php echo $__env-&gt;renderEach(" suffix="); ?&gt;" />
      <data directive="@endwhile" />
      <data directive="@empty" />
      <data directive="@overwrite" />
      <data directive="@endunless" />
      <data directive="@stop" />
      <data directive="@breakpoint" />
      <data directive="@include" injection="true" prefix="&lt;?php echo $__env-&gt;make(" suffix=")-&gt;render(); ?&gt;" />
      <data directive="@set" injection="true" prefix="&lt;?php" suffix="?&gt;" />
      <data directive="@markdown" />
      <data directive="@endpush" />
    </customDirectives>
  </component>
</project>
EOF;

    }
}
