<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Idea\Components;

use BadMethodCallException;
use Sebwite\Idea\Contracts\Idea;
use Sebwite\Support\Filesystem;
use Sebwite\Support\Path;
use Sebwite\Support\Traits\Extendable;

/**
 * This is the class Reader.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 */
abstract class Component
{
    use Extendable;

    /**
     * @var \Illuminate\Filesystem\Filesystem
     */
    protected $fs;

    /**
     * @var string
     */
    protected $path;

    /**
     * @var \SimpleXMLElement
     */
    protected $xml;

    /**
     * @var \Sebwite\Idea\Contracts\Idea|\Sebwite\Idea\Idea
     */
    protected $idea;

    /**
     * Component constructor.
     *
     * @param \Sebwite\Idea\Contracts\Idea|\Sebwite\Idea\Idea $parent
     * @param \Illuminate\Filesystem\Filesystem               $files
     */
    public function __construct(Idea $parent)
    {
        $this->idea = $parent;
        $this->fs   = Filesystem::create();
        $this->path = Path::join($parent->getIdeaPath(), $this->getFileName());

        $this->ensureFile();
    }

    abstract protected function getFileName();

    abstract protected function getRootName();

    protected function getTemplate()
    {
        return "<{$this->getRootName()}></{$this->getRootName()}>";
    }

    protected function fs()
    {
        return $this->fs;
    }

    /**
     * getIdea method
     *
     * @return \Sebwite\Idea\Contracts\Idea|\Sebwite\Idea\Idea
     */
    protected function getIdea()
    {
        return $this->idea;
    }

    public function getContainer()
    {
        return $this->idea->getContainer();
    }

    public function writeTemplateFile()
    {
        $rootName = $this->getRootName();
        $project  = simplexml_load_string($this->getTemplate());
        $this->writeXML($project);
        return $project;
    }

    /**
     * ensureFile method
     */
    protected function ensureFile()
    {
        if (!$this->fs->exists($this->path)) {
            $this->xml = $this->writeTemplateFile();
        } else {
            $this->xml = $this->readXML();
        }
    }

    /**
     * save method
     *
     * @return $this
     */
    public function save()
    {
        $this->writeXML($this->xml);

        return $this;
    }

    /**
     * reload method
     *
     * @return $this
     */
    public function reload()
    {
        $this->xml = $this->readXML();

        return $this;
    }

    /**
     * addComponent method
     *
     * @param $name
     *
     * @return $this
     */
    protected function addComponent($name)
    {
        $this->xml->addChild('component')->addAttribute('name', $name);

        return $this;
    }

    /**
     * getComponent method
     *
     * @param $name
     *
     * @return \SimpleXMLElement
     */
    protected function getComponent($name, $ensure = false)
    {
        if ($ensure === true && !$this->hasComponent($name)) {
            $this->addComponent($name);
        }

        foreach ($this->xml->xpath('//component') as $comp) {
            if ((string)$comp->attributes()->{'name'} == $name) {
                return $comp;
            }
        }
    }

    /**
     * hasComponent method
     *
     * @param $name
     *
     * @return bool
     */
    protected function hasComponent($name)
    {
        foreach ($this->xml->xpath('//component') as $comp) {
            if ((string)$comp->attributes()->{'name'} == $name) {
                return true;
            }
        }

        return false;
    }

    /**
     * readFile method
     *
     * @return string
     * @throws \Illuminate\Contracts\Filesystem\FileNotFoundException
     */
    protected function readFile()
    {
        return $this->fs->get($this->path);
    }

    /**
     * writeFile method
     *
     * @param $data
     *
     * @return $this
     */
    protected function writeFile($data)
    {
        $this->fs->put($this->path, $data);

        return $this;
    }

    /**
     * readXML method
     *
     * @return \SimpleXMLElement
     */
    protected function readXML()
    {
        return simplexml_load_string($this->readFile());
    }

    /**
     * writeXML method
     *
     * @param \SimpleXMLElement $xml
     *
     * @return $this
     */
    protected function writeXML(\SimpleXMLElement $xml)
    {
        $this->writeFile($xml->asXML());

        return $this;
    }

    /**
     * get xml value
     *
     * @return \SimpleXMLElement
     */
    protected function getXml()
    {
        return $this->xml;
    }

    /**
     * Set the xml value
     *
     * @param \SimpleXMLElement $xml
     *
     * @return Component
     */
    protected function setXml($xml)
    {
        $this->xml = $xml;

        return $this;
    }

    protected function getAttributes(\SimpleXMLElement $xml)
    {
        $attrs = [ ];
        foreach ($xml->attributes() as $k => $v) {
            $attrs[ $k ] = (string)$v;
        }

        return $attrs;
    }

    /**
     * parsePath method
     *
     * @param             $path
     * @param null|string $var
     *
     * @return string
     */
    protected function parsePath($path, $var = null)
    {
        if ($var === null) {
            $var = '$' . strtoupper($this->getRootName()) . '_DIR$';
        }
        $path = Path::isRelative($path) ? $path : Path::makeRelative($path, base_path());
        $path = Path::join($var, $path);

        return $path;
    }

    /**
     * cleanPath method
     *
     * @param             $path
     * @param null|string $var
     *
     * @return string
     */
    protected function cleanPath($path, $var = null)
    {
        if ($var === null) {
            $var = '$' . strtoupper($this->getRootName()) . '_DIR$';
        }

        return Path::makeRelative($path, $var);
    }


    public function __call($method, $params)
    {
        $idea = $this->idea;

        if (array_key_exists($method, static::$extensions)) {
            return $this->callExtension($method, $params);
        } elseif (method_exists($idea, $method) || array_key_exists($method, $idea::extensions())) {
            return call_user_func_array([ $idea, $method ], $params);
        }

        throw new BadMethodCallException("Method [$method] does not exist.");
    }
}
