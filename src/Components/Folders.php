<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */
namespace Sebwite\Idea\Components;

use Sebwite\Support\Str;

/**
 * This is the class LaravelPlugin.
 *
 * @package        Sebwite\Phpstorm
 * @author         Sebwite
 * @copyright      Copyright (c) 2015, Sebwite. All rights reserved
 *
 * @mixin \Illuminate\Support\Collection
 */
class Folders extends Component
{
    const RESOURCE = 'resource';
    const SOURCE   = 'source';
    const TEST     = 'test';

    protected function getRootName()
    {
        return 'module';
    }

    public function getFileName()
    {
        return $this->getIdea()->getProjectName() . '.iml';
    }

    protected function getTemplate()
    {
        return <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<module type="JAVA_MODULE" version="4">
  <component name="NewModuleRootManager" inherit-compiler-output="true">
    <exclude-output />
    <content url="file://\$MODULE_DIR$">
    </content>
    <orderEntry type="inheritedJdk" />
    <orderEntry type="sourceFolder" forTests="false" />
    <orderEntry type="module-library">
      <library name="PHP">
        <CLASSES>
        </CLASSES>
        <JAVADOC />
        <SOURCES>
        </SOURCES>
      </library>
    </orderEntry>
  </component>
</module>
EOT;

    }


    /**
     * getContent method
     *
     * @return \SimpleXMLElement
     */
    protected function getContent()
    {
        return $this->getXml()->component->content;
    }

    /**
     * all method
     *
     * @return \Illuminate\Support\Collection
     */
    public function all()
    {
        $folders = $this->getXml()->xpath('//component/content/sourceFolder');


        return collect($folders)->transform(function (\SimpleXMLElement $item) {
        
            $attr   = $this->getAttributes($item);
            $type   = static::SOURCE;
            $prefix = '';
            if (isset($attr[ 'type' ]) && $attr[ 'type' ] === 'java-resource') {
                $type   = static::RESOURCE;
                $prefix = isset($attr[ 'relativeOutputPath' ]) ? $attr[ 'relativeOutputPath' ] : '';
            } elseif (isset($attr[ 'isTestSource' ])) {
                $type   = $attr[ 'isTestSource' ] === 'false' ? static::SOURCE : static::TEST;
                $prefix = isset($attr[ 'packagePrefix' ]) ? $attr[ 'packagePrefix' ] : '';
            }

            return [
                'path'   => $this->cleanPath(Str::removeLeft($attr[ 'url' ], 'file://')),
                'type'   => $type,
                'prefix' => $prefix
            ];
        });
    }

    /**
     * hasPath method
     *
     * @param $path
     *
     * @return bool
     */
    public function hasPath($path)
    {
        return !$this->all()->where('path', $path)->isEmpty();
    }

    /**
     * hasSource method
     *
     * @param $prefix
     */
    public function hasSourcePrefix($prefix)
    {
        $this->has($prefix, static::RESOURCE);
    }

    /**
     * hasTest method
     *
     * @param $prefix
     */
    public function hasTestPrefix($prefix)
    {
        $this->has($prefix, static::RESOURCE);
    }

    /**
     * hasResource method
     *
     * @param $prefix
     */
    public function hasResourcePrefix($prefix)
    {
        $this->has($prefix, static::RESOURCE);
    }

    public function hasPrefix($prefix)
    {
        return !$this->all()->where('prefix', $prefix)->isEmpty();
    }

    /**
     * has method
     *
     * @param        $prefix
     * @param string $type
     *
     * @return bool
     */
    protected function has($prefix, $type = self::SOURCE)
    {
        return !$this->all()->where('type', $type)->where('prefix', $prefix)->isEmpty();
    }

    public function addSource($prefix, $path)
    {
        $this->push($prefix, $path, self::SOURCE);
    }

    public function addTest($prefix, $path)
    {
        $this->push($prefix, $path, self::TEST);
    }

    public function addResource($prefix, $path)
    {
        $this->push($prefix, $path, self::RESOURCE);
    }


    /**
     * push method
     *
     * @param        $prefix
     * @param        $path
     * @param string $type
     *
     * @return $this
     */
    protected function push($prefix, $path, $type = self::SOURCE)
    {
        if ($this->hasPath($path)) {
            return $this;
        }

        /**
         * @var \SimpleXMLElement $child
         */
        $child = $this->getXml()->component->content->addChild('sourceFolder');

        if (in_array($type, [ self::SOURCE, self::TEST ], true)) {
            $child->addAttribute('isTestSource', $type === self::TEST);
            $child->addAttribute('packagePrefix', $prefix);
        } elseif ($type === self::RESOURCE) {
            $child->addAttribute('type', 'java-resource');
            $child->addAttribute('relativeOutputPath', $prefix);
        }
        $child->addAttribute('url', 'file://' . $this->parsePath($path));

        unset($child);

        return $this;
    }

    /**
     * get method
     *
     * @param $path
     *
     * @return mixed
     */
    public function get($path)
    {
        return $this->all()->where('path', $path)->first();
    }

    public function __call($method, $params)
    {
        $collection = $this->all();
        if (method_exists($collection, $method)) {
            return call_user_func_array([ $collection, $method ], $params);
        } else {
            return parent::__call($method, $params);
        }
    }
}
