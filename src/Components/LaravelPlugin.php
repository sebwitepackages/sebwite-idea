<?php
/**
 * Part of the Sebwite PHP packages.
 *
 * License and copyright information bundled with this package in the LICENSE file
 */


namespace Sebwite\Idea\Components;

class LaravelPlugin extends Component
{

    protected function getFileName()
    {
        return 'laravel-plugin.xml';
    }

    protected function getRootName()
    {
        return 'project';
    }

    protected function getTemplate()
    {
        return <<<EOT
<?xml version="1.0" encoding="UTF-8"?>
<project version="4">
  <component name="LaravelPluginSettings">
    <option name="pluginEnabled" value="true" />
    <option name="useAutoPopup" value="true" />
    <option name="templatePaths"><list></list></option>
  </component>
</project>
EOT;
    }



    /**
     * getTemplatePaths method
     *
     * @return \SimpleXMLElement
     */
    protected function getTemplatePaths()
    {
        return $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list/templatePath');
    }

    public function viewPaths()
    {
        $paths = [ ];
        foreach ($this->getTemplatePaths() as $path) {
            $paths[] = $this->getAttributes($path);
        }

        return $paths;
    }


    public function hasPath($path)
    {
        return ! collect($this->viewPaths())->where('path', $path)->isEmpty();
    }

    public function hasNamespace($namespace)
    {
        return ! collect($this->viewPaths())->where('namespace', $namespace)->isEmpty();
    }

    /**
     * getPath method
     *
     * @param $path
     * @return \SimpleXMLElement[]
     */
    public function getPath($path)
    {
        // $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list/templatePath[@path="' . $path . '"]');
        return collect($this->viewPaths())->where('path', $path)->first();
    }

    public function getNamespace($namespace)
    {
        return ! collect($this->viewPaths())->where('namespace', $namespace)->first();
    }

    public function add($namespace, $path)
    {
        if (count($this->getTemplatePaths()) === 0) {
            $option = $this->getComponent('LaravelPluginSettings')->addChild('option');
            $option->addAttribute('name', 'templatePaths');
            $list = $option->addChild('list');
        } else {
            $list = $this->getComponent('LaravelPluginSettings')->xpath('//option[@name="templatePaths"]/list')[ 0 ];
        }
        $child = $list->addChild('templatePath');
        $child->addAttribute('namespace', $namespace);
        $child->addAttribute('path', $path);
    }
}
