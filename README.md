Sebwite Idea
====================

[![Build Status](https://img.shields.io/travis/sebwite/idea.svg?&style=flat-square)](https://travis-ci.org/sebwite/idea)
[![Scrutinizer coverage](https://img.shields.io/scrutinizer/coverage/g/sebwite/idea.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/idea)
[![Scrutinizer quality](https://img.shields.io/scrutinizer/g/sebwite/idea.svg?&style=flat-square)](https://scrutinizer-ci.com/g/sebwite/idea)
[![Source](http://img.shields.io/badge/source-sebwite/idea-blue.svg?style=flat-square)](https://github.com/sebwite/idea)
[![License](http://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square)](http://mit-license.org)

Sebwite Idea is a package for the Laravel 5 framework.

The package follows the FIG standards PSR-1, PSR-2, and PSR-4 to ensure a high level of interoperability between shared PHP code.

Documentation
-------------
Tbd

Quick Installation
------------------
Begin by installing the package through Composer.

```bash
composer require sebwite/idea
```

